﻿// © CC BY-NC-SA 4.0 - 2014 - MysteryDash
// License details can be found here : http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

using System;
using System.Collections.Generic;
using System.IO;

namespace MysteryDash.Brainfuck
{
    /// <summary>
    /// Provides methods to execute or debug a brainfuck program
    /// </summary>
    public sealed class Interpreter
    {
        /// <summary>
        /// The Brainfuck Program
        /// </summary>
        public String Program { get; private set; }
        /// <summary>
        /// Program Counter
        /// </summary>
        public Int32 PC { get; private set; }
        /// <summary>
        /// Stack Used for Loops
        /// </summary>
        private Stack<Int32> LoopStack { get; set; }
        /// <summary>
        /// The Memory Array, Default Size is 30.000 Bytes
        /// </summary>
        public Byte[] Memory { get; private set; }
        /// <summary>
        /// The Memory Pointer
        /// </summary>  
        public Int32 MemoryPtr { get; private set; }
        /// <summary>
        /// The Input Stream, Input Data from ',' Instruction
        /// </summary>
        public TextReader Input { get; private set; }
        /// <summary>
        /// The Output Stream, Output Data from '.' Instruction
        /// </summary>
        public TextWriter Output { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see ref="Interpreter"/> class.
        /// </summary>
        /// <param name="Input">Input Data from ',' Instruction</param>
        /// <param name="Output">Output Data from '.' Instruction</param>
        public Interpreter(TextReader Input, TextWriter Output)
        {
            this.Input = Input;
            this.Output = Output;
        }

        /// <summary>
        /// Load a Brainfuck Program in the Interpreter
        /// </summary>
        /// <param name="BrainfuckProgram">The Brainfuck Program</param>
        /// <param name="CustomMemorySize">The Memory Array Size, Default is 30.000 Bytes</param>
        public void Load(String BrainfuckProgram, Int32 CustomMemorySize = 30000)
        {
            Program = BrainfuckProgram;
            PC = 0;
            LoopStack = new Stack<Int32>();
            Memory = new Byte[CustomMemorySize];
            MemoryPtr = 0;
        }

        /// <summary>
        /// Execute the Next Instruction, Ignore Every Non-Instruction Characters
        /// </summary>
        /// <returns>Return False if EOF</returns>
        public Boolean PerformNextInstruction()
        {
            // Check for the next instruction and ignore every non-instruction characters.
            // Catch an exception if EOF.
            try
            {
                while ("><+-.,[]".IndexOf(Program[PC]) == -1)
                {
                    PC++;
                }

                switch (Program[PC])
                {
                    case '>': // Increase Memory Pointer
                        MemoryPtr++;
                        break;
                    case '<': // Decrease Memory Pointer
                        if (MemoryPtr > 0) MemoryPtr--;
                        break;
                    case '+': // Increase Data at Memory Pointer
                        if (Memory[MemoryPtr] != 0xFF) Memory[MemoryPtr]++;
                        break;
                    case '-': // Decrease Data at Memory Pointer
                        if (Memory[MemoryPtr] != 0x0) Memory[MemoryPtr]--;
                        break;
                    case '.': // Output Data at Memory Pointer
                        Output.Write((Char)Memory[MemoryPtr]);
                        Output.Flush();
                        break;
                    case ',': // Input Data to Memory Pointer
                        Memory[MemoryPtr] = (byte)Math.Max(0, Input.Read());
                        break;
                    case '[': // Loop Start - More Infos on Wikipedia
                        if (Memory[MemoryPtr] == 0x0)
                        {
                            PC++;
                            Int32 InnerLoops = 0;
                            while (Program[PC] != ']' || InnerLoops != 0)
                            {
                                if (Program[PC] == '[')
                                    InnerLoops++;
                                else if (Program[PC] == ']' && InnerLoops > 0)
                                    InnerLoops--;
                                PC++;
                            }
                        }
                        else
                            LoopStack.Push(PC);
                        break;
                    case ']': // Loop End - More Infos on Wikipedia
                        if (Memory[MemoryPtr] != 0x0)
                            PC = LoopStack.Peek();
                        else
                            LoopStack.Pop();
                        break;
                }

                PC++; // Go to the Next Instruction
            }
            catch (IndexOutOfRangeException)
            {
                return false; // EOF
            }

            return true;
        }
    }
}